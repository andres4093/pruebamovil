(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-email-email-module"],{

/***/ "./src/app/pages/email/email.module.ts":
/*!*********************************************!*\
  !*** ./src/app/pages/email/email.module.ts ***!
  \*********************************************/
/*! exports provided: EmailPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmailPageModule", function() { return EmailPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _email_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./email.page */ "./src/app/pages/email/email.page.ts");







var EmailPageModule = /** @class */ (function () {
    function EmailPageModule() {
    }
    EmailPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_5__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["ReactiveFormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild([
                    {
                        path: '',
                        component: _email_page__WEBPACK_IMPORTED_MODULE_6__["EmailPage"]
                    }
                ])
            ],
            declarations: [_email_page__WEBPACK_IMPORTED_MODULE_6__["EmailPage"]]
        })
    ], EmailPageModule);
    return EmailPageModule;
}());



/***/ }),

/***/ "./src/app/pages/email/email.page.html":
/*!*********************************************!*\
  !*** ./src/app/pages/email/email.page.html ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button icon=\"arrow-round-back\"></ion-back-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <form [formGroup]=\"formEmail\">\n    <ion-grid>\n      <ion-row color=\"primary\" justify-content-center>\n        <ion-col align-self-center size-md=\"6\" size-lg=\"5\" size-xs=\"12\">\n          <div text-center>\n            <h3>Enviar Correo</h3>\n            <p>Solo enviar a correos gmail</p>\n          </div>\n          <div padding>\n            <ion-item class=\"input-gral\">\n              <ion-label>\n                <span class=\"fa fa-id-card\"></span>\n              </ion-label>\n              <ion-input type=\"text\" placeholder=\"Para\" formControlName=\"to\" required email></ion-input>\n            </ion-item>\n\n            <ion-item class=\"input-gral\">\n              <ion-label>\n                <span class=\"fa fa-id-card\"></span>\n              </ion-label>\n              <ion-input type=\"text\" placeholder=\"Asunto\" formControlName=\"subject\" required></ion-input>\n            </ion-item>\n\n            <ion-item class=\"input-gral\">\n              <ion-label>\n                <span class=\"fa fa-id-card\"></span>\n              </ion-label>\n              <ion-textarea type=\"text\" placeholder=\"mensaje\" formControlName=\"body\" required></ion-textarea>\n            </ion-item>\n\n          </div>\n          <div padding>\n            <ion-button type=\"submit\" ion-button round full [disabled]=\"formEmail.invalid\" (click)=\"onSubmit()\">Enviar</ion-button>\n          </div>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </form>\n</ion-content>"

/***/ }),

/***/ "./src/app/pages/email/email.page.scss":
/*!*********************************************!*\
  !*** ./src/app/pages/email/email.page.scss ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2VtYWlsL2VtYWlsLnBhZ2Uuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/pages/email/email.page.ts":
/*!*******************************************!*\
  !*** ./src/app/pages/email/email.page.ts ***!
  \*******************************************/
/*! exports provided: EmailPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmailPage", function() { return EmailPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_native_email_composer_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic-native/email-composer/ngx */ "./node_modules/@ionic-native/email-composer/ngx/index.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");





var EmailPage = /** @class */ (function () {
    function EmailPage(emailComposer, formBuilder, alertController, loadingCtrl, navCtrl) {
        this.emailComposer = emailComposer;
        this.formBuilder = formBuilder;
        this.alertController = alertController;
        this.loadingCtrl = loadingCtrl;
        this.navCtrl = navCtrl;
        /* Validacion de formulario */
        this.formEmail = formBuilder.group({
            to: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            subject: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            body: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
        });
        this.sendEmail();
    }
    EmailPage.prototype.onSubmit = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var _a, dataobj;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.loadingCtrl.create({
                                message: "Enviando...",
                            })];
                    case 1:
                        _a.loader = _b.sent();
                        return [4 /*yield*/, this.loader.present()];
                    case 2:
                        _b.sent();
                        if (!(this.formEmail.valid == false)) return [3 /*break*/, 4];
                        return [4 /*yield*/, this.loader.dismiss()];
                    case 3:
                        _b.sent(); // se detiene el loader
                        dataobj = {
                            header: '¡Úps!',
                            message: 'Debes ingresar todos los datos',
                        };
                        this.alert(dataobj); // se envia al metodo para mostrar el mensaje con los parametros deseados
                        return [3 /*break*/, 6];
                    case 4: return [4 /*yield*/, this.loader.dismiss()];
                    case 5:
                        _b.sent(); // se detiene el loader
                        this.sendEmail(); // se envia al metodo para registar usuario
                        _b.label = 6;
                    case 6: return [2 /*return*/];
                }
            });
        });
    };
    /* metodo para enviar el correo */
    EmailPage.prototype.sendEmail = function () {
        var _this = this;
        this.emailComposer.isAvailable().then(function (available) {
            console.log('available ', available);
            _this.emailComposer.hasPermission().then(function (isPermitted) {
                console.log('isPermitted ', isPermitted);
                var email = {
                    app: 'mailto',
                    to: 'andreshernandez4093@gmail.com',
                    from: 'andres4093@gmail.com',
                    subject: 'Prueba',
                    body: 'How are you? Nice greetings from Leipzig',
                    isHtml: true
                };
                _this.emailComposer.open(email); // Envía un mensaje de texto
            }).catch(function (error) {
                console.log('Sin permiso de acceso concedido');
                console.dir(error);
            });
        }).catch(function (error) {
            console.log('El usuario no parece tener una cuenta de correo electrónico en el dispositivo');
            console.dir(error);
        });
    };
    /* Metodo para mostrar mensajes de alerta recibiendo parametros configurables */
    EmailPage.prototype.alert = function (dataobj) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var alert;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertController.create({
                            header: dataobj.header,
                            message: dataobj.message,
                            buttons: ['Verificar']
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    EmailPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["Component"])({
            selector: 'app-email',
            template: __webpack_require__(/*! ./email.page.html */ "./src/app/pages/email/email.page.html"),
            styles: [__webpack_require__(/*! ./email.page.scss */ "./src/app/pages/email/email.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_native_email_composer_ngx__WEBPACK_IMPORTED_MODULE_3__["EmailComposer"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["AlertController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["LoadingController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["NavController"]])
    ], EmailPage);
    return EmailPage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-email-email-module.js.map