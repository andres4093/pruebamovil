(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-add-user-add-user-module"],{

/***/ "./src/app/pages/add-user/add-user.module.ts":
/*!***************************************************!*\
  !*** ./src/app/pages/add-user/add-user.module.ts ***!
  \***************************************************/
/*! exports provided: AddUserPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddUserPageModule", function() { return AddUserPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _add_user_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./add-user.page */ "./src/app/pages/add-user/add-user.page.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");







var routes = [
    {
        path: '',
        component: _add_user_page__WEBPACK_IMPORTED_MODULE_3__["AddUserPage"]
    }
];
var AddUserPageModule = /** @class */ (function () {
    function AddUserPageModule() {
    }
    AddUserPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_6__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)
            ],
            declarations: [_add_user_page__WEBPACK_IMPORTED_MODULE_3__["AddUserPage"]]
        })
    ], AddUserPageModule);
    return AddUserPageModule;
}());



/***/ }),

/***/ "./src/app/pages/add-user/add-user.page.html":
/*!***************************************************!*\
  !*** ./src/app/pages/add-user/add-user.page.html ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n\n  <ion-toolbar>\n    <ion-title>Agregar Especie</ion-title>\n    <ion-button slot=\"start\" ion-button  href=\"/home\" routerDirection=\"root\">\n        <ion-icon name=\"arrow-round-back\"></ion-icon>\n    </ion-button>\n  </ion-toolbar>\n\n</ion-header>\n\n<ion-content>\n  <form  #form=\"ngForm\" (ngSubmit)=\"register(form)\">\n    <ion-grid>\n      <ion-row color=\"primary\" justify-content-center>\n        <ion-col align-self-center size-md=\"6\" size-lg=\"5\" size-xs=\"12\">\n          <div text-center>\n            <h3>Agregar</h3>\n          </div>\n          <div padding>\n            <ion-item>\n              <ion-input type=\"text\" placeholder=\"Código\" [(ngModel)]=\"form.codigo\" ngControl=\"codigo\" name=\"codigo\" required></ion-input>\n            </ion-item>\n            <ion-item>\n              <ion-input  type=\"text\" placeholder=\"Nombre común\" [(ngModel)]=\"form.nombre\" ngControl=\"nombre\" name=\"nombre\" required></ion-input>\n            </ion-item>\n            <ion-item>\n              <ion-input  type=\"text\" placeholder=\"Especie\" [(ngModel)]=\"form.especie\" ngControl=\"especie\" name=\"especie\" required></ion-input>\n            </ion-item>\n            <ion-item>\n              <ion-input  type=\"text\" placeholder=\"CAP\" [(ngModel)]=\"form.cap\" ngControl=\"cap\" name=\"cap\" required></ion-input>\n            </ion-item>\n            <ion-item>\n              <ion-input  type=\"text\" placeholder=\"Altura Total\" [(ngModel)]=\"form.alturaTotal\" ngControl=\"alturaTotal\" name=\"alturaTotal\" required></ion-input>\n            </ion-item>\n            <ion-item>\n              <ion-input  type=\"text\" placeholder=\"Altura Comercial\" [(ngModel)]=\"form.alturaComercial\" ngControl=\"alturaComercial\" name=\"alturaComercial\" required></ion-input>\n            </ion-item>\n            <ion-item>\n              <ion-textarea type=\"text\" placeholder=\"Observaciones\" [(ngModel)]=\"form.observaciones\" ngControl=\"observaciones\" name=\"observaciones\" required></ion-textarea>\n            </ion-item>\n\n\n          </div>\n          <div padding>\n            <ion-button  size=\"large\" type=\"submit\" [disabled]=\"form.invalid\" expand=\"block\">Guardar</ion-button>\n          </div>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </form>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/pages/add-user/add-user.page.scss":
/*!***************************************************!*\
  !*** ./src/app/pages/add-user/add-user.page.scss ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2FkZC11c2VyL2FkZC11c2VyLnBhZ2Uuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/pages/add-user/add-user.page.ts":
/*!*************************************************!*\
  !*** ./src/app/pages/add-user/add-user.page.ts ***!
  \*************************************************/
/*! exports provided: AddUserPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddUserPage", function() { return AddUserPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _services_database_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../../services/database.service */ "./src/app/services/database.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");




var AddUserPage = /** @class */ (function () {
    function AddUserPage(navCtrl, databaseService) {
        this.navCtrl = navCtrl;
        this.databaseService = databaseService;
        this.data = [];
        this.form = {
            codigo: '',
            nombre: '',
            especie: '',
            cap: '',
            alturaTotal: '',
            alturaComercial: '',
            observaciones: '',
        };
    }
    AddUserPage.prototype.register = function (form) {
        console.log(form.value);
        // this.databaseService.insert(form).then(response => {
        //   this.data.unshift( form );
        //   this.navCtrl.navigateRoot('');
        //   console.log(response);
        //   }).catch( error => {
        //     console.error( error );
        // })
    };
    AddUserPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
            selector: 'app-add-user',
            template: __webpack_require__(/*! ./add-user.page.html */ "./src/app/pages/add-user/add-user.page.html"),
            styles: [__webpack_require__(/*! ./add-user.page.scss */ "./src/app/pages/add-user/add-user.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"],
            _services_database_service__WEBPACK_IMPORTED_MODULE_1__["DatabaseService"]])
    ], AddUserPage);
    return AddUserPage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-add-user-add-user-module.js.map