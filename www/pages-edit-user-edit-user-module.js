(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-edit-user-edit-user-module"],{

/***/ "./src/app/pages/edit-user/edit-user.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/pages/edit-user/edit-user.module.ts ***!
  \*****************************************************/
/*! exports provided: EditUserPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditUserPageModule", function() { return EditUserPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _edit_user_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./edit-user.page */ "./src/app/pages/edit-user/edit-user.page.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");







var routes = [
    {
        path: '',
        component: _edit_user_page__WEBPACK_IMPORTED_MODULE_3__["EditUserPage"]
    }
];
var EditUserPageModule = /** @class */ (function () {
    function EditUserPageModule() {
    }
    EditUserPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_6__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_4__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["ReactiveFormsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)
            ],
            declarations: [_edit_user_page__WEBPACK_IMPORTED_MODULE_3__["EditUserPage"]]
        })
    ], EditUserPageModule);
    return EditUserPageModule;
}());



/***/ }),

/***/ "./src/app/pages/edit-user/edit-user.page.html":
/*!*****************************************************!*\
  !*** ./src/app/pages/edit-user/edit-user.page.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button icon=\"arrow-round-back\"></ion-back-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <form [formGroup]=\"Form\">\n    <ion-grid>\n      <ion-row color=\"primary\" justify-content-center>\n        <ion-col align-self-center size-md=\"6\" size-lg=\"5\" size-xs=\"12\">\n          <div text-center>\n            <h3>Registrar</h3>\n          </div>\n          <div padding>\n            <ion-item class=\"input-gral\">\n              <ion-label>\n                <span class=\"fa fa-id-card\"></span>\n              </ion-label>\n              <ion-input type=\"text\" placeholder=\"Nombres\" formControlName=\"name\" value=\"{{name}}\" required></ion-input>\n            </ion-item>\n\n            <ion-item class=\"input-gral\">\n              <ion-label>\n                <span class=\"fa fa-id-card\"></span>\n              </ion-label>\n              <ion-input type=\"text\" placeholder=\"Apellidos\" formControlName=\"last_name\" value=\"{{last_name}}\" required></ion-input>\n            </ion-item>\n\n            <ion-item class=\"input-gral\">\n              <ion-label>\n                <span class=\"fa fa-id-card\"></span>\n              </ion-label>\n              <ion-input type=\"number\" placeholder=\"Identificacion\" formControlName=\"identification\" value=\"{{identification}}\" required></ion-input>\n            </ion-item>\n\n            <ion-item class=\"input-gral\">\n              <ion-label>\n                <span class=\"fa fa-id-card\"></span>\n              </ion-label>\n              <ion-input type=\"number\" placeholder=\"Telefono\" formControlName=\"phone\" value=\"{{phone}}\" required></ion-input>\n            </ion-item>\n\n            <ion-item class=\"input-gral\">\n              <ion-label>\n                <span class=\"fa fa-id-card\"></span>\n              </ion-label>\n              <ion-input type=\"text\" placeholder=\"Correo\" formControlName=\"email\" value=\"{{email}}\" required email></ion-input>\n            </ion-item>\n\n            <!-- <ion-item class=\"input-gral\">\n              <ion-label>\n                <span class=\"fa fa-address-card\"></span>\n                <p>Genero</p>\n              </ion-label>\n              <ion-select formControlName=\"gender\" ok-text=\"Aceptar\" cancel-text=\"Cancelar\">\n                <ion-select-option value=\"Mujer\">Mujer</ion-select-option>\n                <ion-select-option value=\"Mujer\">Hombre</ion-select-option>\n              </ion-select>\n            </ion-item> -->\n\n            <!-- <ion-item class=\"input-gral\">\n              <ion-label>\n                <span class=\"fa fa-address-card\"></span>\n                <p>Pais</p>\n              </ion-label>\n              <ion-select ok-text=\"Aceptar\" cancel-text=\"Cancelar\">\n                <ion-select-option value=\"Mujer\">Colombia</ion-select-option>\n              </ion-select>\n            </ion-item>\n\n            <ion-item class=\"input-gral\">\n              <ion-label>\n                <span class=\"fa fa-address-card\"></span>\n                <p>Departamento</p>\n              </ion-label>\n              <ion-select ok-text=\"Aceptar\" cancel-text=\"Cancelar\">\n                <ion-select-option value=\"Mujer\">Cundinamarca</ion-select-option>\n              </ion-select>\n            </ion-item>\n\n            <ion-item class=\"input-gral\">\n              <ion-label>\n                <span class=\"fa fa-address-card\"></span>\n                <p>Municipio</p>\n              </ion-label>\n              <ion-select formControlName=\"municipality\" ok-text=\"Aceptar\" cancel-text=\"Cancelar\">\n                <ion-select-option value=\"Mujer\">Bogotá</ion-select-option>\n              </ion-select>\n            </ion-item> -->\n\n          </div>\n          <div padding>\n            <ion-button type=\"submit\" ion-button round full (click)=\"onSubmit()\">Guardar</ion-button>\n          </div>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </form>\n</ion-content>"

/***/ }),

/***/ "./src/app/pages/edit-user/edit-user.page.scss":
/*!*****************************************************!*\
  !*** ./src/app/pages/edit-user/edit-user.page.scss ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2VkaXQtdXNlci9lZGl0LXVzZXIucGFnZS5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/pages/edit-user/edit-user.page.ts":
/*!***************************************************!*\
  !*** ./src/app/pages/edit-user/edit-user.page.ts ***!
  \***************************************************/
/*! exports provided: EditUserPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditUserPage", function() { return EditUserPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_database_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../../services/database.service */ "./src/app/services/database.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");






var EditUserPage = /** @class */ (function () {
    function EditUserPage(navCtrl, databaseService, activatedRoute, formBuilder) {
        this.navCtrl = navCtrl;
        this.databaseService = databaseService;
        this.activatedRoute = activatedRoute;
        this.formBuilder = formBuilder;
        this.data = [];
        /* Validacion de formulario */
        this.Form = formBuilder.group({
            name: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('[a-zA-Z `´ñÑáéíóúÁÉÍÓÚ\s]*'), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required])],
            last_name: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('[a-zA-Z `´ñÑáéíóúÁÉÍÓÚ\s]*'), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required])],
            identification: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            phone: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            email: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            gender: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            municipality: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
        });
        /* parametros que se traen para mostrar en el fomrulario */
        this.index = this.activatedRoute.snapshot.paramMap.get('index');
        this.id = this.activatedRoute.snapshot.paramMap.get('id');
        this.name = this.activatedRoute.snapshot.paramMap.get('name');
        this.last_name = this.activatedRoute.snapshot.paramMap.get('last_name');
        this.identification = this.activatedRoute.snapshot.paramMap.get('identification');
        this.phone = this.activatedRoute.snapshot.paramMap.get('phone');
        this.email = this.activatedRoute.snapshot.paramMap.get('email');
        this.gender = this.activatedRoute.snapshot.paramMap.get('gender');
        this.municipality = this.activatedRoute.snapshot.paramMap.get('municipality');
    }
    EditUserPage.prototype.onSubmit = function () {
        var name, last_name, identification, phone, email, gender, municipality;
        /* se valida si tuvo algun cambio los campos */
        if (this.Form.value.name != '') {
            name = this.Form.value.name;
        }
        else {
            name = this.name;
        }
        if (this.Form.value.last_name != '') {
            last_name = this.Form.value.last_name;
        }
        else {
            last_name = this.last_name;
        }
        if (this.Form.value.identification != '') {
            identification = this.Form.value.identification;
        }
        else {
            identification = this.identification;
        }
        if (this.Form.value.phone != '') {
            phone = this.Form.value.phone;
        }
        else {
            phone = this.phone;
        }
        if (this.Form.value.email != '') {
            email = this.Form.value.email;
        }
        else {
            email = this.email;
        }
        if (this.Form.value.gender != '') {
            gender = this.Form.value.gender;
        }
        else {
            gender = this.gender;
        }
        if (this.Form.value.municipality != '') {
            municipality = this.Form.value.municipality;
        }
        else {
            municipality = this.municipality;
        }
        /* data que se envia para actualizar */
        var dataobj = {
            id: this.id,
            name: name,
            last_name: last_name,
            identification: identification,
            phone: phone,
            email: email,
            gender: gender,
            municipality: municipality
        };
        this.update(dataobj, this.index); // envio al metodo de actualizar
    };
    /* Metodo para actualizar un usuario */
    EditUserPage.prototype.update = function (data, index) {
        var _this = this;
        data = Object.assign({}, data);
        data.completed = !data.completed;
        this.databaseService.updateUser(data).then(function (response) {
            console.log(data);
            console.log(response);
            _this.data[index] = data;
            _this.navCtrl.navigateRoot('/home');
        }).catch(function (error) {
            console.error(error);
        });
    };
    EditUserPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_5__["Component"])({
            selector: 'app-edit-user',
            template: __webpack_require__(/*! ./edit-user.page.html */ "./src/app/pages/edit-user/edit-user.page.html"),
            styles: [__webpack_require__(/*! ./edit-user.page.scss */ "./src/app/pages/edit-user/edit-user.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"],
            _services_database_service__WEBPACK_IMPORTED_MODULE_2__["DatabaseService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]])
    ], EditUserPage);
    return EditUserPage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-edit-user-edit-user-module.js.map