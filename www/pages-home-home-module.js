(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-home-home-module"],{

/***/ "./src/app/pages/home/home.module.ts":
/*!*******************************************!*\
  !*** ./src/app/pages/home/home.module.ts ***!
  \*******************************************/
/*! exports provided: HomePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageModule", function() { return HomePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home.page */ "./src/app/pages/home/home.page.ts");







var HomePageModule = /** @class */ (function () {
    function HomePageModule() {
    }
    HomePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_5__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild([
                    {
                        path: '',
                        component: _home_page__WEBPACK_IMPORTED_MODULE_6__["HomePage"]
                    }
                ])
            ],
            declarations: [_home_page__WEBPACK_IMPORTED_MODULE_6__["HomePage"]]
        })
    ], HomePageModule);
    return HomePageModule;
}());



/***/ }),

/***/ "./src/app/pages/home/home.page.html":
/*!*******************************************!*\
  !*** ./src/app/pages/home/home.page.html ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-title>Registros</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n\n<ion-content>\n\n  <ion-infinite-scroll threshold=\"100px\" (ionInfinite)=\"loadData($event)\">\n    <ion-infinite-scroll-content loadingSpinner=\"bubbles\" loadingText=\"Cargando...\">\n\n      <ion-card *ngFor=\"let item of data let i = index\">\n        <ion-item>\n          <ion-buttons slot=\"start\">\n            <ion-button ion-button icon-only (click)=\"editUser(item, i)\">\n              <ion-icon md=\"md-create\"></ion-icon>\n            </ion-button>\n          </ion-buttons>\n\n          <p>{{item.name}} {{item.last_name}}</p>\n\n          <ion-buttons slot=\"end\">\n            <ion-button ion-button icon-only (click)=\"deleteUser(item, i)\">\n              <ion-icon name=\"trash\"></ion-icon>\n            </ion-button>\n          </ion-buttons>\n\n        </ion-item>\n\n        <ion-card-content>\n          <ion-item>\n            <ion-label>Telefono: {{item.identification}}</ion-label>\n          </ion-item>\n\n          <ion-item>\n            <ion-label>Identificacion: {{item.identification}}</ion-label>\n          </ion-item>\n\n          <ion-item>\n            <ion-label>Correo: {{item.email}}</ion-label>\n          </ion-item>\n\n          <ion-item>\n            <ion-label>Genero: {{item.gender}}</ion-label>\n          </ion-item>\n\n          <ion-item>\n            <ion-label>Municipio: {{item.municipality}}</ion-label>\n          </ion-item>\n\n        </ion-card-content>\n      </ion-card>\n\n    </ion-infinite-scroll-content>\n  </ion-infinite-scroll>\n\n</ion-content>\n\n<ion-footer>\n  <ion-toolbar>\n    <ion-tabs>\n      <ion-tab-bar slot=\"bottom\">\n\n        <ion-tab-button (click)=\"register()\">\n          <ion-icon name=\"people\"></ion-icon>\n          <ion-label>Crear usuario</ion-label>\n        </ion-tab-button>\n\n        <ion-tab-button (click)=\"email()\">\n          <ion-icon name=\"mail\"></ion-icon>\n          <ion-label>Enviar correo</ion-label>\n        </ion-tab-button>\n\n        <ion-tab-button (click)=\"logout()\">\n          <ion-icon name=\"exit\"></ion-icon>\n          <ion-label>Cerrar Sesion</ion-label>\n        </ion-tab-button>\n\n      </ion-tab-bar>\n    </ion-tabs>\n  </ion-toolbar>\n</ion-footer>"

/***/ }),

/***/ "./src/app/pages/home/home.page.scss":
/*!*******************************************!*\
  !*** ./src/app/pages/home/home.page.scss ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2hvbWUvaG9tZS5wYWdlLnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/pages/home/home.page.ts":
/*!*****************************************!*\
  !*** ./src/app/pages/home/home.page.ts ***!
  \*****************************************/
/*! exports provided: HomePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePage", function() { return HomePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _services_database_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../../services/database.service */ "./src/app/services/database.service.ts");
/* harmony import */ var _ionic_native_native_storage_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic-native/native-storage/ngx */ "./node_modules/@ionic-native/native-storage/ngx/index.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");






var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, alertController, databaseService, activatedRoute, zone, nativeStorage) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.alertController = alertController;
        this.databaseService = databaseService;
        this.activatedRoute = activatedRoute;
        this.zone = zone;
        this.nativeStorage = nativeStorage;
        this.data = []; // variable donde se almacena toda la data
        /* trae todos los usuarios registrados */
        this.zone.run(function () {
            _this.databaseService.getAllUser().then(function (dataUsers) {
                _this.data = dataUsers;
            }).catch(function (error) {
                console.error(error);
            });
        });
    }
    /* metodo que carga la data en el home */
    HomePage.prototype.loadData = function (event) {
        setTimeout(function () {
            // console.log('Done');
            event.target.complete();
        }, 500);
    };
    /* metodo que permite el scroll infinito */
    HomePage.prototype.toggleInfiniteScroll = function () {
        this.infiniteScroll.disabled = !this.infiniteScroll.disabled;
    };
    /* metodo que envia editar el usuario */
    HomePage.prototype.editUser = function (data, i) {
        this.navCtrl.navigateForward('/edit-user/' + data.id + '/' + data.name + '/' + data.last_name + '/' + data.identification + '/' + data.phone + '/' + data.email + '/' + data.gender + '/' + data.municipality + '/' + i);
    };
    /* metodo para eliminar usuario */
    HomePage.prototype.deleteUser = function (data, index) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertController.create({
                            header: 'Eliminar',
                            message: '¿Deseas eliminar el usuario?',
                            buttons: [
                                {
                                    text: 'Cancelar',
                                    role: 'Cancelar',
                                    cssClass: 'secondary',
                                    handler: function (blah) {
                                        console.log('Confirm Cancel: blah', blah);
                                    }
                                }, {
                                    text: 'Eliminar',
                                    handler: function () {
                                        console.log('Confirm Okay');
                                        _this.databaseService.deleteUser(data).then(function (response) {
                                            console.log(response);
                                            _this.data.splice(index, 1);
                                        }).catch(function (error) {
                                            console.error(error);
                                        });
                                    }
                                }
                            ]
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    /* metodo que envia al formulario de registro */
    HomePage.prototype.register = function () {
        this.navCtrl.navigateForward('/register');
    };
    /* metodo que envia al formulario de correo */
    HomePage.prototype.email = function () {
        this.navCtrl.navigateForward('/email');
    };
    /* metodo que finaliza la sesion */
    HomePage.prototype.logout = function () {
        this.nativeStorage.setItem('user', null); // se elimina la sesion
        this.navCtrl.navigateRoot('/login');
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["ViewChild"])(_ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonInfiniteScroll"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonInfiniteScroll"])
    ], HomePage.prototype, "infiniteScroll", void 0);
    HomePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["Component"])({
            selector: 'app-home',
            template: __webpack_require__(/*! ./home.page.html */ "./src/app/pages/home/home.page.html"),
            styles: [__webpack_require__(/*! ./home.page.scss */ "./src/app/pages/home/home.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_1__["NavController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["AlertController"],
            _services_database_service__WEBPACK_IMPORTED_MODULE_2__["DatabaseService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"],
            _angular_core__WEBPACK_IMPORTED_MODULE_4__["NgZone"],
            _ionic_native_native_storage_ngx__WEBPACK_IMPORTED_MODULE_3__["NativeStorage"]])
    ], HomePage);
    return HomePage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-home-home-module.js.map