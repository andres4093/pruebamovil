(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-users-users-module"],{

/***/ "./src/app/pages/users/users.module.ts":
/*!*********************************************!*\
  !*** ./src/app/pages/users/users.module.ts ***!
  \*********************************************/
/*! exports provided: UsersPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsersPageModule", function() { return UsersPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _users_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./users.page */ "./src/app/pages/users/users.page.ts");







var UsersPageModule = /** @class */ (function () {
    function UsersPageModule() {
    }
    UsersPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_5__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild([
                    {
                        path: '',
                        component: _users_page__WEBPACK_IMPORTED_MODULE_6__["UsersPage"]
                    }
                ])
            ],
            declarations: [_users_page__WEBPACK_IMPORTED_MODULE_6__["UsersPage"]]
        })
    ], UsersPageModule);
    return UsersPageModule;
}());



/***/ }),

/***/ "./src/app/pages/users/users.page.html":
/*!*********************************************!*\
  !*** ./src/app/pages/users/users.page.html ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-title>Usuarios</ion-title>\n    <ion-buttons slot=\"end\">\n      <ion-button ion-button icon-only (click)=\"addData()\">\n        <ion-icon name=\"add-circle\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n\n<ion-content>\n\n  <ion-infinite-scroll threshold=\"100px\" (ionInfinite)=\"loadData($event)\">\n    <ion-infinite-scroll-content loadingSpinner=\"bubbles\" loadingText=\"Cargando...\">\n\n      <ion-card *ngFor=\"let item of data let i = index\">\n        <ion-item>\n          <ion-buttons slot=\"start\">\n            <ion-button ion-button icon-only (click)=\"editData(item, i)\">\n              <ion-icon md=\"md-create\"></ion-icon>\n            </ion-button>\n          </ion-buttons>\n\n          <ion-label>Nombre: {{item.nombre}} </ion-label>\n\n          <ion-buttons slot=\"end\">\n            <ion-button ion-button icon-only (click)=\"deleteData(item, i)\">\n              <ion-icon name=\"trash\"></ion-icon>\n            </ion-button>\n          </ion-buttons>\n\n        </ion-item>\n\n        <ion-card-content>\n          <ion-item>\n            <ion-icon name=\"leaf\" slot=\"start\"></ion-icon>\n            <ion-label>Código: {{item.codigo}}</ion-label>\n          </ion-item>\n\n          <ion-item>\n            <ion-icon name=\"leaf\" slot=\"start\"></ion-icon>\n            <ion-label>CAP: {{item.cap}} cm</ion-label>\n          </ion-item>\n\n          <ion-item>\n            <ion-icon name=\"leaf\" slot=\"start\"></ion-icon>\n            <ion-label>Altura Total: {{item.alturaTotal}} cm</ion-label>\n          </ion-item>\n\n        </ion-card-content>\n      </ion-card>\n\n    </ion-infinite-scroll-content>\n  </ion-infinite-scroll>\n</ion-content>"

/***/ }),

/***/ "./src/app/pages/users/users.page.scss":
/*!*********************************************!*\
  !*** ./src/app/pages/users/users.page.scss ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3VzZXJzL3VzZXJzLnBhZ2Uuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/pages/users/users.page.ts":
/*!*******************************************!*\
  !*** ./src/app/pages/users/users.page.ts ***!
  \*******************************************/
/*! exports provided: UsersPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsersPage", function() { return UsersPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _services_database_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../../services/database.service */ "./src/app/services/database.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");





var UsersPage = /** @class */ (function () {
    function UsersPage(navCtrl, alertController, databaseService, activatedRoute, zone) {
        this.navCtrl = navCtrl;
        this.alertController = alertController;
        this.databaseService = databaseService;
        this.activatedRoute = activatedRoute;
        this.zone = zone;
        this.data = [];
        this.zone.run(function () {
            // this.databaseService.getAll().then(dataDiana => {
            //   this.data = dataDiana;
            // }).catch( error => {
            //   console.error( error );
            // });
        });
    }
    UsersPage.prototype.loadData = function (event) {
        setTimeout(function () {
            console.log('Done');
            event.target.complete();
            // App logic to determine if all data is loaded
            // and disable the infinite scroll
            // if (data.length == 1000) {
            //   event.target.disabled = true;
            // }
        }, 500);
    };
    UsersPage.prototype.toggleInfiniteScroll = function () {
        this.infiniteScroll.disabled = !this.infiniteScroll.disabled;
    };
    UsersPage.prototype.addData = function () {
        this.navCtrl.navigateForward('/add-user');
    };
    UsersPage.prototype.editData = function (data, i) {
        this.navCtrl.navigateForward('/edit-user/' + data.id + '/' + data.codigo + '/' + data.nombre + '/' + data.especie + '/' + data.cap + '/' + data.alturaTotal + '/' + data.alturaComercial + '/' + data.observaciones + '/' + i);
    };
    UsersPage.prototype.deleteData = function (data, index) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var alert;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertController.create({
                            header: 'Eliminar',
                            message: '¿Deseas eliminar el usuario?',
                            buttons: [
                                {
                                    text: 'Cancelar',
                                    role: 'Cancelar',
                                    cssClass: 'secondary',
                                    handler: function (blah) {
                                        console.log('Confirm Cancel: blah', blah);
                                    }
                                }, {
                                    text: 'Eliminar',
                                    handler: function () {
                                        console.log('Confirm Okay');
                                        // this.databaseService.delete(data).then(response => {
                                        //   console.log( response );
                                        //   this.data.splice(index, 1);
                                        // }).catch( error => {
                                        //   console.error( error );
                                        // })
                                    }
                                }
                            ]
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["ViewChild"])(_ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonInfiniteScroll"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonInfiniteScroll"])
    ], UsersPage.prototype, "infiniteScroll", void 0);
    UsersPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
            selector: 'app-users',
            template: __webpack_require__(/*! ./users.page.html */ "./src/app/pages/users/users.page.html"),
            styles: [__webpack_require__(/*! ./users.page.scss */ "./src/app/pages/users/users.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_1__["NavController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["AlertController"],
            _services_database_service__WEBPACK_IMPORTED_MODULE_2__["DatabaseService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"],
            _angular_core__WEBPACK_IMPORTED_MODULE_3__["NgZone"]])
    ], UsersPage);
    return UsersPage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-users-users-module.js.map