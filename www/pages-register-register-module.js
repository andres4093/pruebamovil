(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-register-register-module"],{

/***/ "./src/app/pages/register/register.module.ts":
/*!***************************************************!*\
  !*** ./src/app/pages/register/register.module.ts ***!
  \***************************************************/
/*! exports provided: RegisterPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterPageModule", function() { return RegisterPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _register_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./register.page */ "./src/app/pages/register/register.page.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");







var RegisterPageModule = /** @class */ (function () {
    function RegisterPageModule() {
    }
    RegisterPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_6__["ReactiveFormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild([
                    {
                        path: '',
                        component: _register_page__WEBPACK_IMPORTED_MODULE_5__["RegisterPage"]
                    }
                ])
            ],
            declarations: [_register_page__WEBPACK_IMPORTED_MODULE_5__["RegisterPage"]]
        })
    ], RegisterPageModule);
    return RegisterPageModule;
}());



/***/ }),

/***/ "./src/app/pages/register/register.page.html":
/*!***************************************************!*\
  !*** ./src/app/pages/register/register.page.html ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button icon=\"arrow-round-back\"></ion-back-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <form [formGroup]=\"formRegister\">\n    <ion-grid>\n      <ion-row color=\"primary\" justify-content-center>\n        <ion-col align-self-center size-md=\"6\" size-lg=\"5\" size-xs=\"12\">\n          <div text-center>\n            <h3>Registrar</h3>\n          </div>\n          <div padding>\n            <ion-item class=\"input-gral\">\n              <ion-label>\n                <span class=\"fa fa-id-card\"></span>\n              </ion-label>\n              <ion-input type=\"text\" placeholder=\"Nombres\" formControlName=\"name\" required></ion-input>\n            </ion-item>\n\n            <ion-item class=\"input-gral\">\n              <ion-label>\n                <span class=\"fa fa-id-card\"></span>\n              </ion-label>\n              <ion-input type=\"text\" placeholder=\"Apellidos\" formControlName=\"last_name\" required></ion-input>\n            </ion-item>\n\n            <ion-item class=\"input-gral\">\n              <ion-label>\n                <span class=\"fa fa-id-card\"></span>\n              </ion-label>\n              <ion-input type=\"number\" placeholder=\"Identificacion\" formControlName=\"identification\" required></ion-input>\n            </ion-item>\n\n            <ion-item class=\"input-gral\">\n              <ion-label>\n                <span class=\"fa fa-id-card\"></span>\n              </ion-label>\n              <ion-input type=\"number\" placeholder=\"Telefono\" formControlName=\"phone\" required></ion-input>\n            </ion-item>\n\n            <ion-item class=\"input-gral\">\n              <ion-label>\n                <span class=\"fa fa-id-card\"></span>\n              </ion-label>\n              <ion-input type=\"text\" placeholder=\"Correo\" formControlName=\"email\" required email></ion-input>\n            </ion-item>\n\n            <ion-item class=\"input-gral\">\n              <ion-label>\n                <span class=\"fa fa-id-card\"></span>\n              </ion-label>\n              <ion-input type=\"password\" placeholder=\"Contraseña\" formControlName=\"password\"></ion-input>\n            </ion-item>\n\n            <ion-item class=\"input-gral\">\n              <ion-label>\n                <span class=\"fa fa-id-card\"></span>\n              </ion-label>\n              <ion-input type=\"password\" placeholder=\"Confirmar Contraseña\" formControlName=\"confirpassword\"></ion-input>\n            </ion-item>\n\n            <ion-item class=\"input-gral\">\n              <ion-label>\n                <span class=\"fa fa-address-card\"></span>\n                <p>Genero</p>\n              </ion-label>\n              <ion-select formControlName=\"gender\" ok-text=\"Aceptar\" cancel-text=\"Cancelar\">\n                <ion-select-option value=\"Mujer\">Mujer</ion-select-option>\n                <ion-select-option value=\"Hombre\">Hombre</ion-select-option>\n              </ion-select>\n            </ion-item>\n\n            <ion-item class=\"input-gral\">\n              <ion-label>\n                <span class=\"fa fa-address-card\"></span>\n                <p>Pais</p>\n              </ion-label>\n              <ion-select ok-text=\"Aceptar\" cancel-text=\"Cancelar\">\n                <ion-select-option value=\"Colombia\">Colombia</ion-select-option>\n              </ion-select>\n            </ion-item>\n\n            <ion-item class=\"input-gral\">\n              <ion-label>\n                <span class=\"fa fa-address-card\"></span>\n                <p>Departamento</p>\n              </ion-label>\n              <ion-select ok-text=\"Aceptar\" cancel-text=\"Cancelar\">\n                <ion-select-option value=\"Cundinamarca\">Cundinamarca</ion-select-option>\n              </ion-select>\n            </ion-item>\n\n            <ion-item class=\"input-gral\">\n              <ion-label>\n                <span class=\"fa fa-address-card\"></span>\n                <p>Municipio</p>\n              </ion-label>\n              <ion-select formControlName=\"municipality\" ok-text=\"Aceptar\" cancel-text=\"Cancelar\">\n                <ion-select-option value=\"Bogotá\">Bogotá</ion-select-option>\n              </ion-select>\n            </ion-item>\n\n          </div>\n          <div padding>\n            <ion-button type=\"submit\" ion-button round full [disabled]=\"formRegister.invalid\" (click)=\"onSubmit()\">Ingresar</ion-button>\n          </div>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </form>\n</ion-content>"

/***/ }),

/***/ "./src/app/pages/register/register.page.scss":
/*!***************************************************!*\
  !*** ./src/app/pages/register/register.page.scss ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3JlZ2lzdGVyL3JlZ2lzdGVyLnBhZ2Uuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/pages/register/register.page.ts":
/*!*************************************************!*\
  !*** ./src/app/pages/register/register.page.ts ***!
  \*************************************************/
/*! exports provided: RegisterPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterPage", function() { return RegisterPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _services_database_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../../services/database.service */ "./src/app/services/database.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");





var RegisterPage = /** @class */ (function () {
    function RegisterPage(formBuilder, dataBase, alertController, loadingCtrl, navCtrl) {
        this.formBuilder = formBuilder;
        this.dataBase = dataBase;
        this.alertController = alertController;
        this.loadingCtrl = loadingCtrl;
        this.navCtrl = navCtrl;
        /* Validacion de formulario */
        this.formRegister = formBuilder.group({
            name: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].pattern('[a-zA-Z `´ñÑáéíóúÁÉÍÓÚ\s]*'), _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required])],
            last_name: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].pattern('[a-zA-Z `´ñÑáéíóúÁÉÍÓÚ\s]*'), _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required])],
            identification: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            phone: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            email: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            gender: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            municipality: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            password: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            confirpassword: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
        });
    }
    /* Metodo de validacion */
    RegisterPage.prototype.onSubmit = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var _a, dataobj, dataobj;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.loadingCtrl.create({
                                message: "Registrando...",
                            })];
                    case 1:
                        _a.loader = _b.sent();
                        return [4 /*yield*/, this.loader.present()];
                    case 2:
                        _b.sent();
                        if (!(this.formRegister.valid == false)) return [3 /*break*/, 4];
                        return [4 /*yield*/, this.loader.dismiss()];
                    case 3:
                        _b.sent(); // se detiene el loader
                        dataobj = {
                            header: '¡Úps!',
                            message: 'Debes ingresar todos los datos',
                        };
                        this.alert(dataobj); // se envia al metodo para mostrar el mensaje con los parametros deseados
                        return [3 /*break*/, 8];
                    case 4:
                        if (!(this.formRegister.value.password === this.formRegister.value.confirpassword)) return [3 /*break*/, 6];
                        return [4 /*yield*/, this.loader.dismiss()];
                    case 5:
                        _b.sent(); // se detiene el loader
                        this.addUser(); // se envia al metodo para registar usuario
                        return [3 /*break*/, 8];
                    case 6: return [4 /*yield*/, this.loader.dismiss()];
                    case 7:
                        _b.sent(); // se detiene el loader
                        dataobj = {
                            header: 'Autenticación',
                            message: 'Usuario ó contraseña incorrectos',
                        };
                        this.alert(dataobj); // se envia al metodo para mostrar el mensaje con los parametros deseados
                        _b.label = 8;
                    case 8: return [2 /*return*/];
                }
            });
        });
    };
    /* Metodo para agregar un usuario nuevo */
    RegisterPage.prototype.addUser = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var dataobj;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                dataobj = {
                    name: this.formRegister.value.name,
                    last_name: this.formRegister.value.last_name,
                    identification: this.formRegister.value.identification,
                    phone: this.formRegister.value.phone,
                    email: this.formRegister.value.email,
                    gender: this.formRegister.value.gender,
                    municipality: this.formRegister.value.municipality,
                    password: this.formRegister.value.password,
                };
                /* se almacena en la base de datos al usuario */
                this.dataBase.addUser(dataobj).then(function (data) {
                    // se recupera el registro
                    if (data.insertId) {
                        _this.navCtrl.navigateRoot('/home'); // luego de registrar se envia al home
                    }
                    else {
                        var dataobj_1 = {
                            header: '¡Úps!',
                            message: 'No se pudo guardar el registro',
                        };
                        _this.alert(dataobj_1); // se envia al metodo para mostrar el mensaje con los parametros deseados
                    }
                });
                return [2 /*return*/];
            });
        });
    };
    /* Metodo para mostrar mensajes de alerta recibiendo parametros configurables */
    RegisterPage.prototype.alert = function (dataobj) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var alert;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertController.create({
                            header: dataobj.header,
                            message: dataobj.message,
                            buttons: ['Verificar']
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    RegisterPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["Component"])({
            selector: 'app-register',
            template: __webpack_require__(/*! ./register.page.html */ "./src/app/pages/register/register.page.html"),
            styles: [__webpack_require__(/*! ./register.page.scss */ "./src/app/pages/register/register.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"],
            _services_database_service__WEBPACK_IMPORTED_MODULE_2__["DatabaseService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["AlertController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["LoadingController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["NavController"]])
    ], RegisterPage);
    return RegisterPage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-register-register-module.js.map