export class User {
    id              : number; // id del usuario
    name            : string; // Nombres del usuario
    last_name       : string; // Apellidos del usuario
    identification  : number; // Identificacion del usuario
    phone           : number; // Telefono del usuario
    gender          : string; // Genero del usuario
    municipality    : string; // Municipio del usuario
    email           : string; // Correo del usuario para posterior ingreso a la app
}
