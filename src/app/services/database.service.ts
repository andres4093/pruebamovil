import { SQLiteObject } from '@ionic-native/sqlite/ngx';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class DatabaseService {

  private database: SQLiteObject = null;

  constructor(){}

  /* metodod para setear la base de datos */
  public setDatabase(db: SQLiteObject){
    if(this.database === null){
      this.database = db;
    }
  }

  /* Creacion tabla usuario */
  public createTable(){
    const sql = 'create table IF NOT EXISTS users (' +
               'id INTEGER PRIMARY KEY AUTOINCREMENT, '+
               'name VARCHAR(255), '+
               'last_name VARCHAR(200), '+
               'identification VARCHAR(200), '+
               'phone VARCHAR(50), '+
               'gender VARCHAR(50), '+
               'municipality VARCHAR(50), '+
               'email VARCHAR(60), '+
               'password VARCHAR(30))';
    return this.database.executeSql(sql, []);
  }


  /* Crea el usuario */
  public addUser(data: any) {
    let sql = 'INSERT INTO users(id,name,last_name,identification,phone,gender,municipality,email,password) VALUES(null,?,?,?,?,?,?,?,?)';
    return this.database.executeSql(sql,[data.name,data.last_name,data.identification,data.phone,data.gender,data.municipality,data.email,data.password]);
  }

  /* Elimina un usuario */
  public deleteUser(data: any){
    let sql = 'DELETE FROM users WHERE id=?';
    return this.database.executeSql(sql, [data.id]);
  }

  /* Mostrar todos los usuarios */
  public getAllUser(){
      let sql = 'SELECT * FROM users ORDER BY id ASC';
      return this.database.executeSql(sql, []).then( response => {
        let data = [];
        for (let index = 0; index < response.rows.length; index++) {
          data.push( response.rows.item(index) );
        }
      return Promise.resolve( data );
    }).catch(error => Promise.reject(error));
  }

  /* Actualizar usuario */
  public updateUser(data: any){
    let sql = 'UPDATE users SET name=?,last_name=?,identification=?,phone=?,gender=?,municipality=? WHERE id=?';
    return this.database.executeSql(sql, [data.name,data.last_name,data.identification,data.phone,data.gender,data.municipality,data.id]);
  }

  /* Mostrar todos los usuarios */
  public authUser(data: any){
      let sql = 'SELECT * FROM users WHERE email=? AND password=?';
      return this.database.executeSql(sql, [data.email, data.password]).then( response => {
        let data = [];
        for (let index = 0; index < response.rows.length; index++) {
          data.push( response.rows.item(index) );
        }
      return Promise.resolve( data );
    }).catch(error => Promise.reject(error));
  }

}
