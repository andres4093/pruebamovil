import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DatabaseService } from './../../services/database.service';
import { ActivatedRoute } from '@angular/router';
import { NavController } from '@ionic/angular';
import { Component } from '@angular/core';


@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.page.html',
  styleUrls: ['./edit-user.page.scss'],
})
export class EditUserPage {

  public data              : any[] = [];
  public index             : any;
  public id                : any;
  public name              : any;
  public last_name         : any;
  public identification    : any;
  public phone             : any;
  public email             : any;
  public gender            : any;
  public municipality      : any;
  public Form              : FormGroup; // formulario


  constructor(public  navCtrl          : NavController,
              public  databaseService  : DatabaseService,
              public  activatedRoute   : ActivatedRoute,
              public  formBuilder	     : FormBuilder,
            ) {

              /* Validacion de formulario */
              this.Form   = formBuilder.group({
                  name            : ['', Validators.compose([Validators.pattern('[a-zA-Z `´ñÑáéíóúÁÉÍÓÚ\s]*'), Validators.required])], // Validador de campo names
                  last_name       : ['', Validators.compose([Validators.pattern('[a-zA-Z `´ñÑáéíóúÁÉÍÓÚ\s]*'), Validators.required])], // Validador de campo last_name
                  identification  : ['', Validators.required],
                  phone           : ['', Validators.required],
                  email           : ['', Validators.required],
                  gender          : ['', Validators.required],
                  municipality    : ['', Validators.required],
              });

              /* parametros que se traen para mostrar en el fomrulario */
              this.index            = this.activatedRoute.snapshot.paramMap.get('index');
              this.id               = this.activatedRoute.snapshot.paramMap.get('id');
              this.name             = this.activatedRoute.snapshot.paramMap.get('name');
              this.last_name        = this.activatedRoute.snapshot.paramMap.get('last_name');
              this.identification   = this.activatedRoute.snapshot.paramMap.get('identification');
              this.phone            = this.activatedRoute.snapshot.paramMap.get('phone');
              this.email            = this.activatedRoute.snapshot.paramMap.get('email');
              this.gender           = this.activatedRoute.snapshot.paramMap.get('gender');
              this.municipality     = this.activatedRoute.snapshot.paramMap.get('municipality');
  }

  onSubmit(){

    let name, last_name, identification, phone, email, gender, municipality;

    /* se valida si tuvo algun cambio los campos */
    if(this.Form.value.name!=''){ name = this.Form.value.name; }
    else{ name = this.name; }

    if(this.Form.value.last_name!=''){ last_name = this.Form.value.last_name; }
    else{ last_name = this.last_name; }

    if(this.Form.value.identification!=''){ identification = this.Form.value.identification; }
    else{ identification = this.identification; }

    if(this.Form.value.phone!=''){ phone = this.Form.value.phone; }
    else{ phone = this.phone; }

    if(this.Form.value.email!=''){ email = this.Form.value.email; }
    else{ email = this.email; }

    if(this.Form.value.gender!=''){ gender = this.Form.value.gender; }
    else{ gender = this.gender; }

    if(this.Form.value.municipality!=''){ municipality = this.Form.value.municipality; }
    else{ municipality = this.municipality; }


    /* data que se envia para actualizar */
    let dataobj = {
        id               : this.id,
        name             : name,
        last_name        : last_name,
        identification   : identification,
        phone            : phone,
        email            : email,
        gender           : gender,
        municipality     : municipality
    }

    this.update(dataobj,this.index); // envio al metodo de actualizar
  }

  /* Metodo para actualizar un usuario */
  private update(data,index){
      data = Object.assign({}, data);
      data.completed = !data.completed;
      this.databaseService.updateUser(data).then( response => {
        console.log(data);
        console.log(response);
        this.data[index] = data;
        this.navCtrl.navigateRoot('/home');
      }).catch( error => {
        console.error( error );
      })

  }
}
