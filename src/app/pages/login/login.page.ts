import { AlertController, NavController, LoadingController } from '@ionic/angular';
import { DatabaseService } from './../../services/database.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { User } from '../../interfaces/user';
import { Component } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: 'login.page.html',
  styleUrls: ['login.page.scss'],
})
export class LoginPage {

  public formLogin : FormGroup;  // Formulario de registro
  public loader    : any;        // loader

  constructor(public formBuilder      : FormBuilder,
              public dataBase         : DatabaseService,
              public alertController  : AlertController,
              public  loadingCtrl     : LoadingController,
              public navCtrl          : NavController,
              private nativeStorage   : NativeStorage,
             ) {

    /* Validacion de formulario */
    this.formLogin   = formBuilder.group({
        email          : ['', Validators.required],
        password       : ['', Validators.required]
    });

  }

  /* Metodo de validacion */
  public async onSubmit(){
    this.loader = await this.loadingCtrl.create({
      message : "Ingresando...",
    });
    await this.loader.present();

    if(this.formLogin.valid==false){
        await this.loader.dismiss(); // se detiene el loader
        const dataobj = {
          header  : '¡Úps!',
          message : 'Debes ingresar todos los datos',
        }
        this.alert(dataobj); // se envia al metodo para mostrar el mensaje con los parametros deseados
    }else{
        await this.loader.dismiss(); // se detiene el loader
        this.auth() // se envia al metodo de auth
    }
  }

  /* Metodo de autenticacion */
  private async auth(){

    /* data que tiene la validacion del usuario */
    const dataobj = {
        email       : this.formLogin.value.email,     // parametro email que se recibe por el formulario de ingreso
        password    : this.formLogin.value.password,  // parametro password que se recibe por el formulario de ingreso
    };

    /* se verifica en la base de datos la existe del usuario */
    this.dataBase.authUser(dataobj).then( data => {
      if(data!=undefined){

        let user = new User; // se instancia la clase usuario

        user.id               = data[0].id
        user.name             = data[0].name;
        user.last_name        = data[0].last_name;
        user.email            = data[0].identification;
        user.identification   = data[0].phone;
        user.phone            = data[0].email;
        user.gender           = data[0].gender;
        user.municipality     = data[0].password;

        this.nativeStorage.setItem('user', user.id); // guardar la sesion
        this.navCtrl.navigateRoot('/home'); // luego de validar autenticacion se envia al home
      }else{

        const dataobj = {
          header  : 'Autenticación',
          message : 'Usuario ó contraseña incorrectos',
        }
        this.alert(dataobj); // se envia al metodo para mostrar el mensaje con los parametros deseados
     }
   },error=>{
     console.log('Error omeeee ..... ',error);
       const dataobj = {
         header  : 'Autenticación',
         message : 'Usuario ó contraseña incorrectos',
       }
       this.alert(dataobj); // se envia al metodo para mostrar el mensaje con los parametros deseados
   });
  }

  /* metodo que redirecciona a la vista de registro */
  public register(){
    this.navCtrl.navigateForward('/register');
  }

  /* Metodo para mostrar mensajes de alerta recibiendo parametros configurables */
  private async alert(dataobj){
      const alert = await this.alertController.create({
      header  : dataobj.header,
      message : dataobj.message,
      buttons : ['Verificar']
     });
     await alert.present();
  }

}
