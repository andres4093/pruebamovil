import { AlertController, NavController, LoadingController } from '@ionic/angular';
import { DatabaseService } from './../../services/database.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component } from '@angular/core';


@Component({
  selector: 'app-register',
  templateUrl: 'register.page.html',
  styleUrls: ['register.page.scss'],
})

export class RegisterPage {

  public formRegister : FormGroup;  // Formulario de registro
  public loader       : any;        // loader

  constructor(public  formBuilder       : FormBuilder,
              private dataBase          : DatabaseService,
              private alertController   : AlertController,
              public  loadingCtrl       : LoadingController,
              public  navCtrl           : NavController,) {

    /* Validacion de formulario */
    this.formRegister   = formBuilder.group({
        name            : ['', Validators.compose([Validators.pattern('[a-zA-Z `´ñÑáéíóúÁÉÍÓÚ\s]*'), Validators.required])], // Validador de campo names
        last_name       : ['', Validators.compose([Validators.pattern('[a-zA-Z `´ñÑáéíóúÁÉÍÓÚ\s]*'), Validators.required])], // Validador de campo last_name
        identification  : ['', Validators.required],
        phone           : ['', Validators.required],
        email           : ['', Validators.required],
        gender          : ['', Validators.required],
        municipality    : ['', Validators.required],
        password        : ['', Validators.required],
        confirpassword  : ['', Validators.required],
    });

  }

  /* Metodo de validacion */
  public async onSubmit(){

    this.loader = await this.loadingCtrl.create({
      message : "Registrando...",
    });
    await this.loader.present();

    if(this.formRegister.valid==false){
        await this.loader.dismiss(); // se detiene el loader
        const dataobj = {
          header  : '¡Úps!',
          message : 'Debes ingresar todos los datos',
        }
        this.alert(dataobj); // se envia al metodo para mostrar el mensaje con los parametros deseados
    }else{
        if(this.formRegister.value.password===this.formRegister.value.confirpassword){
            await this.loader.dismiss(); // se detiene el loader
            this.addUser() // se envia al metodo para registar usuario
        }else{
          await this.loader.dismiss(); // se detiene el loader
          const dataobj ={
            header  : 'Autenticación',
            message : 'Usuario ó contraseña incorrectos',
          }
          this.alert(dataobj); // se envia al metodo para mostrar el mensaje con los parametros deseados
        }
    }
  }

  /* Metodo para agregar un usuario nuevo */
  private async addUser() {

    /* objeto que tiene toda la data del registro */
    const dataobj = {
        name              : this.formRegister.value.name,
        last_name         : this.formRegister.value.last_name,
        identification    : this.formRegister.value.identification,
        phone             : this.formRegister.value.phone,
        email             : this.formRegister.value.email,
        gender            : this.formRegister.value.gender,
        municipality      : this.formRegister.value.municipality,
        password          : this.formRegister.value.password,
    };

    /* se almacena en la base de datos al usuario */
    this.dataBase.addUser(dataobj).then( data => {
      // se recupera el registro
      if(data.insertId){
        this.navCtrl.navigateRoot('/home'); // luego de registrar se envia al home
      }else{
        const dataobj = {
          header  : '¡Úps!',
          message : 'No se pudo guardar el registro',
        }
        this.alert(dataobj); // se envia al metodo para mostrar el mensaje con los parametros deseados
      }
    });
  }


  /* Metodo para mostrar mensajes de alerta recibiendo parametros configurables */
  private async alert(dataobj){
      const alert = await this.alertController.create({
      header  : dataobj.header,
      message : dataobj.message,
      buttons : ['Verificar']
     });
     await alert.present();
  }

}
