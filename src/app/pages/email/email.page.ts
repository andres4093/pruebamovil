import { AlertController, NavController, LoadingController } from '@ionic/angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EmailComposer } from '@ionic-native/email-composer/ngx';
import { User } from '../../interfaces/user';
import { Component } from '@angular/core';


@Component({
  selector: 'app-email',
  templateUrl: 'email.page.html',
  styleUrls: ['email.page.scss'],
})
export class EmailPage {

  public formEmail    : FormGroup;  // Formulario de registro
  public loader       : any;        // loader
  public user         = User;       // se llama la instancia del usuario para validar si existe login

  constructor(private emailComposer     : EmailComposer,
              public  formBuilder       : FormBuilder,
              private alertController   : AlertController,
              public  loadingCtrl       : LoadingController,
              public  navCtrl           : NavController,) {

    /* Validacion de formulario */
    this.formEmail   = formBuilder.group({
        to        : ['', Validators.required],
        subject   : ['', Validators.required],
        body      : ['', Validators.required],
    });

    this.sendEmail();
  }

  public async onSubmit(){

    this.loader = await this.loadingCtrl.create({
      message : "Enviando...",
    });
    await this.loader.present();

    if(this.formEmail.valid==false){
        await this.loader.dismiss(); // se detiene el loader
        const dataobj = {
          header  : '¡Úps!',
          message : 'Debes ingresar todos los datos',
        }
        this.alert(dataobj); // se envia al metodo para mostrar el mensaje con los parametros deseados
    }else{
        await this.loader.dismiss(); // se detiene el loader
        this.sendEmail() // se envia al metodo para registar usuario
    }
  }

  /* metodo para enviar el correo */
  private sendEmail(){
    /* funcion que verifica que se tenga un correo en el dispositivo */
    this.emailComposer.isAvailable().then((available: boolean) =>{
      console.log('available ',available);

      /* funcion que solicita el permiso al correo */
      this.emailComposer.hasPermission().then((isPermitted : boolean) =>{
        console.log('isPermitted ',isPermitted);

         let email = {
          app       : 'gmail',
          to        : this.formEmail.value.to,      // a quien se envia el correo
          from      : this.user[0].email,           // correo donde sale el mensaje
          subject   : this.formEmail.value.subject, // asunto del correo
          body      : this.formEmail.value.body,    // cuerpo del correo
          isHtml    : true
        }

        console.log(email) // datos del mensaje;
        this.emailComposer.open(email); // Envía un mensaje de texto

      }).catch((error : any) => {
            console.log('Sin permiso de acceso concedido ',error);
      });
    }).catch((error : any) => {
         console.log('El usuario no parece tener una cuenta de correo electrónico en el dispositivo ',error);
    });
  }

  /* Metodo para mostrar mensajes de alerta recibiendo parametros configurables */
  private async alert(dataobj){
      const alert = await this.alertController.create({
      header  : dataobj.header,
      message : dataobj.message,
      buttons : ['Verificar']
     });
     await alert.present();
  }
}
