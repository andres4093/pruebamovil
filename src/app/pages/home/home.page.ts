import { NavController, IonInfiniteScroll, AlertController } from '@ionic/angular';
import { DatabaseService } from './../../services/database.service';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { Component, ViewChild, NgZone} from '@angular/core';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  data: any[] = []; // variable donde se almacena toda la data

  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;

    constructor(public  navCtrl          : NavController,
                public  alertController  : AlertController,
                public  databaseService  : DatabaseService,
                public  activatedRoute   : ActivatedRoute,
                public  zone             : NgZone,
                private nativeStorage    : NativeStorage,
    ) {

      /* trae todos los usuarios registrados */
      this.zone.run(() => {
          this.databaseService.getAllUser().then(dataUsers => {
            this.data = dataUsers;
          }).catch( error => {
            console.error( error );
          });
      });
  }

  /* metodo que carga la data en el home */
  public loadData(event) {
    setTimeout(() => {
      // console.log('Done');
      event.target.complete();
      }, 500);
  }

  /* metodo que permite el scroll infinito */
  public toggleInfiniteScroll() {
    this.infiniteScroll.disabled = !this.infiniteScroll.disabled;
  }

  /* metodo que envia editar el usuario */
  public editUser(data, i) {
    this.navCtrl.navigateForward('/edit-user/'+data.id+'/'+data.name+'/'+data.last_name+'/'+data.identification+'/'+data.phone+'/'+data.email+'/'+data.gender+'/'+data.municipality+'/'+i);
  }

  /* metodo para eliminar usuario */
  public async deleteUser(data: any, index) {

    const alert = await this.alertController.create({
      header  : 'Eliminar',
      message : '¿Deseas eliminar el usuario?',
      buttons : [
        {
          text      : 'Cancelar',
          role      : 'Cancelar',
          cssClass  : 'secondary',
          handler   : (blah) => {
            console.log('Confirm Cancel: blah', blah);
          }
        },{
          text    : 'Eliminar',
          handler : () => {
            console.log('Confirm Okay');
            this.databaseService.deleteUser(data).then(response => {
              console.log( response );
              this.data.splice(index, 1);
            }).catch( error => {
              console.error( error );
            })
          }
        }
      ]
    });

    await alert.present();
  }

  /* metodo que envia al formulario de registro */
  public register(){
    this.navCtrl.navigateForward('/register');
  }

  /* metodo que envia al formulario de correo */
  public email(){
    this.navCtrl.navigateForward('/email');
  }

  /* metodo que finaliza la sesion */
  public logout(){
    this.nativeStorage.setItem('user', null); // se elimina la sesion
    this.navCtrl.navigateRoot('/login');
  }

}
