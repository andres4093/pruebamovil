import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { DatabaseService } from './services/database.service';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Platform, NavController } from '@ionic/angular';
import { SQLite } from '@ionic-native/sqlite/ngx';
import { Component, NgZone } from '@angular/core';
import { User } from './interfaces/user';


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {

  public user = User; // se llama la instancia del usuario para validar si existe login

  constructor(private platform      : Platform,
              private splashScreen  : SplashScreen,
              private statusBar     : StatusBar,
              public  dataBase      : DatabaseService,
              private sqlite        : SQLite,
              public  navCtrl       : NavController,
              private zone          : NgZone,
              private nativeStorage : NativeStorage,
  ) {
    this.platform.ready().then(() => {
      this.createDataBase();
      this.initializeApp();
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.zone.run(() => {
        this.nativeStorage.getItem('user').then( data => {
          console.log('---- ***',data);
          if(data!=null){
            this.navCtrl.navigateRoot('/home');  // se redirecciona a la vista de home porque ya tiene sesion
          }else{
            this.navCtrl.navigateRoot('/login'); // se redirecciona a la vista de login
          }
        },error => {
          console.log(error);
          this.navCtrl.navigateRoot('/login'); // se redirecciona a la vista de login
        });
      });
    });
  }

  /* Metodo para crear la base de datos en el dispositivo */
  public createDataBase(){
    this.platform.ready().then(() => {
      this.sqlite.create({
        name     : 'prueba.db',   // Nombre de la base de datos en el dispositivo
        location : 'default'      // Lugar donde se guardara la base de datos en el dispositivo
      }).then((db) => {
        this.dataBase.setDatabase(db);       // seteada la base de datos creada
        return this.dataBase.createTable();  // crea la tabla en el dispositivo
      }).catch(error =>{
        console.error(error);
      });
    });
  }
}
