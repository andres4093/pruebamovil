import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'home',     loadChildren: './pages/home/home.module#HomePageModule' },
  { path: 'email',    loadChildren: './pages/email/email.module#EmailPageModule' },
  { path: 'login',    loadChildren: './pages/login/login.module#LoginPageModule' },
  { path: 'register', loadChildren: './pages/register/register.module#RegisterPageModule' },
  { path: 'edit-user/:id/:name/:last_name/:identification/:phone/:email/:gender/:municipality/:i', loadChildren: './pages/edit-user/edit-user.module#EditUserPageModule' },
];


@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
